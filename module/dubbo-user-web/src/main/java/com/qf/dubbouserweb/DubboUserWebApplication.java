package com.qf.dubbouserweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(scanBasePackages = "com.qf" ,exclude = DataSourceAutoConfiguration.class)
public class DubboUserWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboUserWebApplication.class, args);
    }

}
