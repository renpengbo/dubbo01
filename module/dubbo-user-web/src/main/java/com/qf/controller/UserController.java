package com.qf.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qf.entity.User;
import com.qf.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
public class UserController {

    //@Autowired
    @Reference   //到注册中心去找依赖
    private IUserService userService;
    @RequestMapping(value = "/getUserList")
     public String getUserList(ModelMap map){
        List<User> userList = userService.getAll();
        map.put("list",userList);
        return "userList";
     }

}
