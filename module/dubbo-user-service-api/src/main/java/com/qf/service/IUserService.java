package com.qf.service;

import com.qf.baseservice.IBaseService;
import com.qf.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface IUserService extends IBaseService<User> {

}
