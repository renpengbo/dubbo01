package com.qf.dubboempweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(scanBasePackages = "com.qf", exclude = DataSourceAutoConfiguration.class)
public class DubboEmpWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboEmpWebApplication.class, args);
    }

}
