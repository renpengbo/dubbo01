package com.qf.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qf.entity.Emp;
import com.qf.service.IEmpService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class EmpController {

    @Reference   //到注册中心去找依赖
    public IEmpService empService;

    @RequestMapping(value = "/getEmpList")
    public String getEmpList(){
        List<Emp> empList = empService.getAll();
        System.out.println(empList);
        return "empList";
    }
}
