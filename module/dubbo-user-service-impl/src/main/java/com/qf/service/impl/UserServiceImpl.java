package com.qf.service.impl;


import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.qf.baseservice.impl.BaseServiceImpl;
import com.qf.entity.User;
import com.qf.mapper.IUserMapper;
import com.qf.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;

@Service   //用dubbo提供的注解,这个注解表示在dubbo包下扫描
public class UserServiceImpl extends BaseServiceImpl<User> implements IUserService {

    @Autowired
    private IUserMapper userMapper;

     @Override
     protected BaseMapper getBaseMapper() {
        return userMapper;
     }
}
