package com.qf.service.impl;


import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.qf.baseservice.impl.BaseServiceImpl;
import com.qf.entity.Emp;
import com.qf.mapper.IEmpMapper;
import com.qf.service.IEmpService;

@Service
public class EmpServiceImpl extends  BaseServiceImpl<Emp> implements IEmpService{

    @Reference
    private IEmpMapper empMapper;

    @Override
    protected BaseMapper getBaseMapper() {
        return empMapper;
    }
}
