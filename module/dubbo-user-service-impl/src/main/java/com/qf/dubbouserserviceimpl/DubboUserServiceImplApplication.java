package com.qf.dubbouserserviceimpl;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@DubboComponentScan(basePackages = "com.qf")
@MapperScan(basePackages = "com.qf.mapper")
public class DubboUserServiceImplApplication {


    public static void main(String[] args) {
        SpringApplication.run(DubboUserServiceImplApplication.class, args);
    }

}
