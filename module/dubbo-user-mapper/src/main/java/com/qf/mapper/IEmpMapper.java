package com.qf.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.qf.entity.Emp;

public interface IEmpMapper extends BaseMapper<Emp> {
}
