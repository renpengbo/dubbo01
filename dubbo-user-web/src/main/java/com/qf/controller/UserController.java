package com.qf.controller;


import com.qf.entity.User;
import com.qf.service.IUserService;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;




@Controller
public class UserController {

  //  @Autowired
    @Reference   //发现服务
    private IUserService userService;

    @RequestMapping(value="/getUserList")
    public String getUserList(ModelMap map) {
        List<User> list = userService.getAll();
        map.put("list",list);
        return "userList";
    }


}
