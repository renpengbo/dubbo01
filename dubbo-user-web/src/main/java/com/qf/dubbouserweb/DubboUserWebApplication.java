package com.qf.dubbouserweb;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(scanBasePackages = "com.qf",exclude = DataSourceAutoConfiguration.class)
@DubboComponentScan(basePackages = "com.qf")
public class DubboUserWebApplication {

    public static void main(String[] args) {

        SpringApplication.run(DubboUserWebApplication.class, args);

    }

}
