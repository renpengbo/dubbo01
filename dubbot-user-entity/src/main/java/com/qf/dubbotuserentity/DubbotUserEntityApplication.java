package com.qf.dubbotuserentity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DubbotUserEntityApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubbotUserEntityApplication.class, args);
    }

}
