package com.qf.dubbouserserviceimpl;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication()
@MapperScan(basePackages = "com.qf.mapper")
@DubboComponentScan(basePackages = "com.qf")
public class DubboUserServiceImplApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboUserServiceImplApplication.class, args);
    }

}
