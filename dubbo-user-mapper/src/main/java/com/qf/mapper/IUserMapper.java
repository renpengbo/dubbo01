package com.qf.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;

import com.qf.entity.User;
import org.springframework.stereotype.Repository;


@Repository
public interface IUserMapper extends BaseMapper<User> {




}
